package powershell;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Vector;

import app.model.Exclusions;

public class Command 
{
	public static String execute(String cmd)
	{

		String command = "powershell.exe $("+cmd+")";
		String output = "";
		try 
		{
			Process powerShellProcess = Runtime.getRuntime().exec(command);
			powerShellProcess.getOutputStream().close();
			System.out.println("Powershell : " + cmd);

			Thread errorGobbler
		      = new Thread(new ErrorGobbler(powerShellProcess.getErrorStream()));
		    Thread outputGobbler
		      = new Thread(new StreamGobbler(powerShellProcess.getInputStream()));
		    
		    
		    ByteArrayOutputStream baos = new ByteArrayOutputStream();
		    PrintStream ps = new PrintStream(baos);
		    PrintStream old = System.out;
		    System.setOut(ps);
		    
		    errorGobbler.start();
		    outputGobbler.start();
		    powerShellProcess.waitFor();
		    errorGobbler.join();   // Handle condition where the
		    outputGobbler.join();  // process ends before the threads finish
		    System.out.flush();
		    System.setOut(old);
		    output=baos.toString();
			System.out.println(output);
			
		}
		catch (Exception e) 
		{
			e.printStackTrace();
		}
		return output;
	}
	
	public static Vector<String> getComputers()
	{
		Vector<String> names = new Vector<String>();
		String res = execute("Get-ADComputer -Filter * | Select -Expand Name");
		String buffer = "";
		for ( int i = 0 ; i < res.length() ; i++)
		{
			if ( res.charAt(i) == '\\' )
			{
				if ( ! Exclusions.isExcluded(buffer) && buffer.indexOf("ERRORPOWERSHELL") == -1)
					names.addElement(buffer);
				buffer = "";
			}
			else
			{
				buffer += res.charAt(i);
			}
		}
		return names;
	}
	
	public static  String renameComputers(ArrayList<String> names, ArrayList<String> oldNames)
	{
		String output="";
		String cmd = "$cred = Get-Credential;\n";
		for ( int i = 0 ; i < names.size() ; i ++ )
			if ( i+1 < 100 )
				cmd += "Rename-computer –computername "+ oldNames.get(i) + " –newname "+ names.get(i)  +" –domaincredential $cred –force –restart;\n";

		output = execute(cmd);
		
		output = output.replace("\\","\n");
		
		return output;
	}
	
	//Get-ADComputer -Filter  {OperatingSystem -notLike '*SERVER*' } -Properties lastlogontimestamp,operatingsystem |select name,lastlogondate,operatingsystem
	
	static class StreamGobbler implements Runnable {
		  private final InputStream is;
		  StreamGobbler(InputStream is) {
		    this.is = is;
		  }
		 
		  public void run() {
		    try {
		      int c;
		      while ((c = is.read()) != -1)
		      {
		    	  if ( (char) c !=  '\n' )
		    		  System.out.print((char) c);
		    	  else 
		    		  System.out.print("\\");
		      }
		    } catch (IOException x) {
		      // Handle error
		    }
		  }
		}
	
	static class ErrorGobbler implements Runnable {
		  private final InputStream is;
		  ErrorGobbler(InputStream is) {
		    this.is = is;
		  }
		 
		  public void run() {
		    try {
		      int c;
		      while ((c = is.read()) != -1)
		      {
		    	  if ( (char) c !=  '\n' )
		    		  System.out.print((char) c);
		    	  else
		    		  System.out.print(" [ERRORPOWERSHELL] \\ ");
		      }
		      
		    } catch (IOException x) {
		      // Handle error
		    }
		  }
		}
	//Rename-computer –computername “computer” –newname “newcomputername” –domaincredential domain-user –force –restart
}
