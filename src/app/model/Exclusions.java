package app.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;

public class Exclusions implements Serializable
{
	private static final long serialVersionUID = 1L;
	private static ArrayList<String> excludedComputer = new ArrayList<String>();
	
	
	public static boolean isExcluded(String name)
	{
		for ( String t : excludedComputer )
		{
			if ( t.equals(name))
				return true;
		}
		return false;
	}
	
	public static void exclude(ArrayList<String> newExclusions)
	{
		load();
		if ( newExclusions != null )
		{
			if ( excludedComputer == null)
				excludedComputer = new ArrayList<String>();
			for ( String c : newExclusions )
			{
				if ( ! isExcluded(c) )
					excludedComputer.add(c);
			}
		}
		Collections.sort(excludedComputer);
		save();
	}
	
	public static void remove(ArrayList<String> names)
	{
		load();
		for ( String c : names)
			excludedComputer.remove(c);
		save();
	}
	
	public static ArrayList<String> getExcluded()
	{
		load();
		return excludedComputer;
	}
	
	public static boolean save()
	{
        try 
        {
            FileOutputStream fileOut = new FileOutputStream(".\\ServMan_exclusions.data");
            ObjectOutputStream objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(excludedComputer);
            objectOut.close();
            return true;
        } 
        catch (Exception ex) 
        {
            ex.printStackTrace();
        }
        return false;
	}
	
	@SuppressWarnings("unchecked")
	public static boolean load()
	{
		 try 
		 {
			 FileInputStream streamIn = new FileInputStream(".\\ServMan_exclusions.data");
		     ObjectInputStream objectInputStream = new ObjectInputStream(streamIn);
		     excludedComputer = (ArrayList<String>) objectInputStream.readObject();
		     
		     for ( String c  : excludedComputer )
		    	 System.out.println(c);
		     
		     objectInputStream.close();
		     return true;
		 }
		 catch (Exception e) 
		 {
			 save();
		     e.printStackTrace();
		 }
		 return false;
	}

}
