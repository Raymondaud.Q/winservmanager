package app.interfacegraphique;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Vector;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListModel;
import javax.swing.SwingUtilities;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;

import app.model.Exclusions;
import powershell.Command;

public class RenameView extends JPanel 
{
	private static final long serialVersionUID = 1L;
	
	final public static String NAME = "Rename View";
	
	private JPanel newPanel;						// Panneau dans lequel on entrera le pattern ou les nouveaux noms par PC
	private JPanel actionPanel;						// Panneau dans lequel sera afficher tous pc de l'ue									
	private ArrayList<JTextField> individualNames; 	// Contiendra les champs de textuel pour chaques ordinateurs
	private ArrayList<String> currentNames; 		// Contiendra les noms actuels
	private ArrayList<String> newNames; 			// Contiendra les nouveaux noms des PC
	private int selectedOption; 					// Si -1 : Pas encore choisie ; 1 : Individuel ; 2 : Pattern
	private View view;
	private Vector<String> computers;
	private JButton shiftSelection;

	public RenameView (View view)
	{
		super(new FlowLayout(FlowLayout.CENTER, 500, 10));
		this.view=view;
		JFrame parent = (JFrame) SwingUtilities.getWindowAncestor(this);
		JButton home = new JButton(new GoToAction("Menu principal", MenuView.NAME,view));	
		add(home);
		if ( actionPanel != null)
			remove(actionPanel);
		actionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 10));
        setBorder(BorderFactory.createTitledBorder("Renommer des ordinateurs"));
        Font searchFont = new Font("SansSerif", Font.BOLD, 20);
        JTextField filter = new JTextField();
		JLabel filterLab = new JLabel(" Filtres sur les noms de postes ");
		computers = new Vector<String>();
		Vector<String> compute = Command.getComputers();
		
		if ( compute.size() == 0 )
		{
			JOptionPane.showMessageDialog(view,"Aucun poste trouv� dans l'Active Directory.","ECHEC : Aucun poste trouv� ",JOptionPane.WARNING_MESSAGE);
		}
		
		for ( int i = 0 ; i < compute.size() ; i ++)
		{
			if ( ! Exclusions.isExcluded(compute.get(i)))
			{
				computers.add(compute.get(i));
				Collections.sort(computers);
			}
		}
		JList<String> ADcomputers = new JList<String>(computers);
		JScrollPane scrollComputer = new JScrollPane(ADcomputers);
		
		if ( computers.size()*18 < 400 )
			scrollComputer.setPreferredSize(new Dimension(200,computers.size()*18));
		else
			scrollComputer.setPreferredSize(new Dimension(200,450));

		actionPanel.add(scrollComputer);
		
		// INDIVIDUEL
		JButton individualRename = new JButton("Renommage Individuel");
		// PATTERN 
		JButton patternRename = new JButton("Renommage par Pattern");
		// EXCLURE SELECTION
		JButton excludeSelection = new JButton("Exclure/Masquer les �l�ments ");
		
		shiftSelection = new JButton("Inverser deux postes");
		shiftSelection.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				shift(ADcomputers,filter.getText());
			}
		});
		
		excludeSelection.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent event) 
			{
				int[] selectedIx = ADcomputers.getSelectedIndices();
				
				ArrayList<String> excluded =  new ArrayList<String>();
				
				for (int i = 0; i < selectedIx.length; i++) 
			    {
			      String sel = ADcomputers.getModel().getElementAt(selectedIx[i]);
			      excluded.add(sel);
			    }
				Exclusions.exclude(excluded);
				home.doClick();
			}
		});
		
		ADcomputers.addListSelectionListener(new ListSelectionListener() 
		{

		    @Override
		    public void valueChanged(ListSelectionEvent e) 
		    {
		    	System.out.println(selectedOption);
		    	if ( selectedOption == 2)
		    		patternRename.doClick();
		    	if ( selectedOption == 1)
		    		individualRename.doClick();
		    }
		});
		
		add(filterLab);
		add(filter);
		add(actionPanel);
		individualOption(parent,ADcomputers, scrollComputer, individualRename , patternRename);
		patternOption(parent,ADcomputers, scrollComputer, patternRename, individualRename);
		
		
		filter.setPreferredSize(new Dimension(300,40));
		filter.setFont(searchFont);
		filter.getDocument().addDocumentListener(new DocumentListener() 
		{
			 public void changedUpdate(DocumentEvent e) 
			 {
				update();
			 }
			 public void removeUpdate(DocumentEvent e) 
			 {
				update();
			 }
			 public void insertUpdate(DocumentEvent e)
			 {
				update();
			 }

			 public void update() 
			 {
			  	filtrer(ADcomputers,filter.getText());
			  	if ( selectedOption == 2)
					patternRename.doClick();
			  	if ( selectedOption == 1)
					individualRename.doClick();
			 }
		});
		
		selectedOption = -1;
		add(individualRename);
		add(patternRename);
		add(excludeSelection);
		
		view.add(this);
		setVisible(true);
	}
	private void shift(JList<String> ADcomputers, String filtre)
	{
		if ( ADcomputers.getModel().getSize() != 0 )
		{
			int i = ADcomputers.getSelectedIndices()[0];
			int j = i+1;
			if (  i >= ADcomputers.getModel().getSize()-1 )
				j = 0;
			else if ( ADcomputers.getSelectedIndices().length > 1  )
				j = ADcomputers.getSelectedIndices()[1];
			
			ListModel<String> normal = ADcomputers.getModel();
			String fS = normal.getElementAt(i);
			String bS = normal.getElementAt(j);
			DefaultListModel<String> shifted = new DefaultListModel<String>();
			for ( int in = 0 ; in < normal.getSize() ; in ++ )
			{
				if ( in == i)
					shifted.addElement(bS);
				else if ( in == j )
					shifted.addElement(fS);
				else
					shifted.addElement(normal.getElementAt(in));
			}
			
			ADcomputers.setModel(shifted);
			ADcomputers.setSelectedIndex(j);
			repaint();
			revalidate();
		}
	}
	
	private void filtrer(JList<String> ADcomputers, String text) 
	{
		System.out.println(" GO ");
		DefaultListModel<String>  filtered = new DefaultListModel<String>();
		for ( String c : computers)
		{
			
			if ( (text!= null && c != null && c.toLowerCase().indexOf(text.toLowerCase()) != -1 ) || text.equals(""))
			{
				System.out.println(c);
				filtered.addElement(c);
			}
		}
		ADcomputers.setModel(filtered);
		repaint();
		revalidate();
		
	}

	
	public void patternOption (JFrame parent, JList<String> ADcomputers, JScrollPane scrollComputer, JButton groupRename , JButton individualRename )
	{
		
		System.out.println(selectedOption);
		JLabel example= new JLabel("Visualisation impossible");
		groupRename.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				int[] selectedIx = ADcomputers.getSelectedIndices();
				individualRename.setVisible(true);
				groupRename.setVisible(false);
				selectedOption = 2;
				individualNames = null;
				currentNames =  new ArrayList<String>();
				
				for (int i = 0; i < selectedIx.length; i++) 
			    {
					if ( i+1 == 100)
						break;
			    	String sel = ADcomputers.getModel().getElementAt(selectedIx[i]);
			    	currentNames.add(sel);
			    }
				
				if ( newPanel != null)
				{
					newPanel.removeAll();
					actionPanel.removeAll();
					remove(newPanel);
					actionPanel.add(scrollComputer);
				}
				
				newPanel = new JPanel();
				newPanel.setLayout(new BoxLayout(newPanel, BoxLayout.Y_AXIS));
				JTextField pattern = new JTextField();
				pattern.getDocument().addDocumentListener(new DocumentListener() 
				{
					 public void changedUpdate(DocumentEvent e) 
					 {
						update();
					 }
					 public void removeUpdate(DocumentEvent e) 
					 {
						update();
					 }
					 public void insertUpdate(DocumentEvent e)
					 {
						update();
					 }

					 public void update() 
					 {
					    example.setText(OptionView.patternProvider(pattern.getText())); // Update visu
					 }
				});
				pattern.addKeyListener(new KeyAdapter() 
				{
					@Override
					public void keyTyped(KeyEvent e) 
					{
					    if ( pattern.getText().length() >= 13 ) // limit to 13 characters
					    	pattern.setText(pattern.getText().substring(0, 12));
					}
				});
				
				JButton rename = new JButton("Valider");
				rename.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent event)
					{
						if( ! pattern.getText().equals("") && pattern.getText().length() > 0 )
						{
							newNames = new ArrayList<String>();
							for ( int i = 0 ; i < currentNames.size() ; i ++ )
								newNames.add( OptionView.patternProvider(pattern.getText(),(OptionView.startIndex+i)));

							
						}
						else
						{
							JOptionPane.showMessageDialog(parent,"Veuillez saisir au moins une lettre pour composer le pattern","Saisie vide",JOptionPane.WARNING_MESSAGE);
							return;
						}
						
						if ( currentNames != null  && newNames.size() > 0 && currentNames.size() > 0  )
							new ValidationBox(parent,view,currentNames,newNames);
						else
						{
							JOptionPane.showMessageDialog(parent,"Veuillez selectionner au moins un poste � renommer","Aucun poste s�l�ctionn�",JOptionPane.WARNING_MESSAGE);
						}
					}
				});
				actionPanel.add(new JLabel("Nouveau pattern de noms :"));
				newPanel.add(pattern);
				newPanel.add(rename);
				newPanel.add(example);
				newPanel.add(shiftSelection);
				actionPanel.add(newPanel);
				pattern.setPreferredSize(new Dimension(150,20));
				repaint();
				revalidate();
			}
		});
		
	}
	
	public void individualOption (JFrame parent, JList<String> ADcomputers, JScrollPane scrollComputer, JButton individualRename , JButton patternRename)
	{
		System.out.println(selectedOption);
		individualRename.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				selectedOption = 1; 
				patternRename.setVisible(true);
				individualRename.setVisible(false);
				individualNames = new ArrayList<JTextField>();
				currentNames =  new ArrayList<String>();
				newNames = new ArrayList<String>();
				
				int[] selectedIx = ADcomputers.getSelectedIndices();
				if ( newPanel != null)
				{
					newPanel.removeAll();
					remove(newPanel);
					actionPanel.removeAll();
					actionPanel.add(scrollComputer);
				}
				newPanel = new JPanel();
				newPanel.setLayout(new BoxLayout(newPanel, BoxLayout.Y_AXIS));
	
			    // Get all the selected items using the indices
			    for (int i = 0; i < selectedIx.length; i++) 
			    {
					 String sel = ADcomputers.getModel().getElementAt(selectedIx[i]);
					 currentNames.add(sel);
					 newPanel.add(new JLabel("Ancien nom : " + sel));
					 
					 JTextField inputName = new JTextField("");
					 inputName.addKeyListener(new KeyAdapter() 
					 {
						@Override
						public void keyTyped(KeyEvent e) 
						{
						    if ( inputName.getText().length() >= 15 ) // limit to 3 characters
						    	inputName.setText(inputName.getText().substring(0, 14));
						}
					 });
					 individualNames.add(inputName);
					 newPanel.add(individualNames.get(i));
					 System.out.println(sel);
			    }
			    JScrollPane newScroll = new JScrollPane(newPanel);
			    
			    JButton rename = new JButton("Valider");
			    rename.addActionListener(new ActionListener()
				{
					@Override
					public void actionPerformed(ActionEvent event)
					{
						boolean emptyCase = false;
						newNames = new ArrayList<String>();
						for ( int i = 0 ; i < currentNames.size() ; i ++ )
						{
							newNames.add(individualNames.get(i).getText());
							if ( newNames.get(i).equals(""))
							{
								emptyCase = true;
								break;
							}
							System.out.println("Ancien nom : " + currentNames.get(i) + " Nouveau : " + newNames.get(i));
						}
						if ( areValid() && newNames.size() > 0 && ! emptyCase )
							new ValidationBox(parent,view,currentNames,newNames);
						else
						{
							if ( emptyCase )
								JOptionPane.showMessageDialog(parent,"Veuillez saisir un nouveau nom pour chaques ordinateur, sinon dirigez vous vers le renommage par Pattern.","Nouveau nom ind�fini ",JOptionPane.WARNING_MESSAGE);
							else if ( newNames.size() < 1 )
								JOptionPane.showMessageDialog(parent,"Veuillez selectionner au moins un poste � renommer","Aucun poste s�l�ctionn�",JOptionPane.WARNING_MESSAGE);
							else
								JOptionPane.showMessageDialog(parent,"Veuillez saisir des noms de postes distincts et se composant de plus d'un caract�re","Erreur dans les noms",JOptionPane.WARNING_MESSAGE);
						}
					}
				});
			    newScroll.setPreferredSize(new Dimension(200,450));
			    actionPanel.add(newScroll);
			    actionPanel.add(rename);
			    actionPanel.add(shiftSelection);
			    repaint();
			    revalidate();
			}
		});
	}

	public boolean areValid()
	{
		for ( int i = 0; i < newNames.size() ; i ++  )
		{
			for ( int j = 0; j < newNames.size() ; j ++   )
			{
				if ( ( i != j && newNames.get(i).equals(newNames.get(j)) ) || newNames.get(i).length() < 2 )
					return false;
			}
		}
		return true;
	}
}
