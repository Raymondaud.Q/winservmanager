package app.interfacegraphique;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSlider;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;

public class OptionView extends JPanel
{
	private static final long serialVersionUID = 1L;
	public static final String NAME = "Option View";
	private JPanel actionPanel;
	private JLabel exampleLabel = new JLabel("SalleBatiment##");
	public static int cut = 13;
	public static int startIndex = 1 ;
	
	public OptionView (View view)
	{
		super(new FlowLayout(FlowLayout.CENTER, 500, 20));
		
		JButton home = new JButton(new GoToAction("Menu principal", MenuView.NAME,view));	
		add(home);
		if ( actionPanel != null)
			remove(actionPanel);
		actionPanel = new JPanel();
		actionPanel.setLayout(new BoxLayout(actionPanel,BoxLayout.Y_AXIS));
		JSlider patternConfig = new JSlider(JSlider.HORIZONTAL, 0 , 13 , cut);
		showExample();
		patternConfig.addChangeListener(new SliderListener());
		patternConfig.setPreferredSize(new Dimension(300, 80));
		Font fieldFont = new Font("Times", Font.BOLD, 25);
		exampleLabel.setFont(fieldFont);
		
		
		patternConfig.setMajorTickSpacing(1);
		patternConfig.setMinorTickSpacing(1);
		patternConfig.setPaintTicks(true);
		patternConfig.setPaintLabels(true);
		actionPanel.add(new JLabel("Position de l'incr�ment en renommage par Pattern"));
		actionPanel.add(patternConfig);
		
		actionPanel.add(new JLabel(" Chiffre de d�part de l'increment en renommage par pattern :"));
		Vector <Integer> start = new Vector<Integer>();
		for ( int i = 1 ; i < 100 ; i++)
		{
			start.add(i);
		}
		
		JComboBox<Integer> begin = new JComboBox <Integer>(start);
		begin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) 
            {
            	startIndex = (Integer) begin.getSelectedItem();
            	showExample();
            }
        });
		begin.setSelectedItem(startIndex);
		actionPanel.add(begin);
		actionPanel.add(exampleLabel);	
		add(actionPanel);
        setBorder(BorderFactory.createTitledBorder("Options"));
	}
	
	public void showExample()
	{
		
		String nameExample = "SalleBatiment";
		exampleLabel.setText(patternProvider(nameExample));
		repaint();
	}
	
	public static String patternProvider (String nameExample, int i)
	{
		
		String increment = "";
		if (i < 10 )
			increment = "0"+(i);
		else
			increment = (i)+"";
		
		int cutI = cut;
		
		if ( cutI > nameExample.length()  )
			cutI = nameExample.length();
		
		String newName = "";
		if ( cutI == 0 )
			newName += increment+nameExample;
		else if (cutI == 13 )
			newName += nameExample+increment;
		else
			newName += nameExample.substring(0,cutI)+increment+nameExample.substring(cutI);
		return newName;
	}
	
	public static String patternProvider( String nameExample)
	{
		int cutI = cut;
		
		if ( cutI > nameExample.length()  )
			cutI = nameExample.length();
		
		String increment = "";
		if (startIndex < 10 )
			increment = "0"+(startIndex);
		else
			increment = (startIndex)+"";
		
		String newName = "Ex : ";
		if ( cutI == 0 )
			newName += increment+nameExample;
		else if (cutI == 13 )
			newName += nameExample+increment;
		else
			newName += nameExample.substring(0,cutI)+increment+nameExample.substring(cutI);
		return newName;
	}

	class SliderListener implements ChangeListener 
	{
	    public void stateChanged(ChangeEvent e) 
	    {
	        JSlider source = (JSlider)e.getSource();
	        cut = (int)source.getValue();
	        showExample();
	    }
	}
	
	
}
