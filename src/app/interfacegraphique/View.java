package app.interfacegraphique;
import java.awt.CardLayout;
import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.JPanel;

public class View extends JPanel
{
	private static final long serialVersionUID = 1L;
	static final int PREF_W = 900;
	static final int PREF_H = 720;
	private CardLayout cardLayout = new CardLayout();
	private MenuView menuView = new MenuView(this);
	private RenameView renameView ;
	private ExcludeView excludeView = new ExcludeView(this);
	private FreeRenameView freeRenameView =  new FreeRenameView(this);
	private OptionView optionView = new OptionView(this);
	private AnalyzeView analyzeView ;
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		repaint();
		revalidate();
	}
	/**
	 * @author Quentin Raymondaud
	 * Create a new View and add all inner panels
	 */
	public View()
	{
		super(null);
		setLayout(cardLayout);
		add(menuView, MenuView.NAME);
	}

	@Override
	public Dimension getPreferredSize()
	{
		if (isPreferredSizeSet())
		{
			return super.getPreferredSize();
		}
		else
		{
			return new Dimension(PREF_W, PREF_H);
		}
	}

	/**
	 * @author Quentin Raymondaud
	 * [showCard description]
	 * Function called by GoToAction to show a precise Panel
	 * @param key String : name of view
	 */
	public void showCard(String key)
	{
		System.out.println(key);
		
		if ( key.equals("Exclude View"))
		{
			excludeView.removeAll();
			remove(excludeView);
			excludeView=new ExcludeView(this);
			add(excludeView,ExcludeView.NAME);
		}

		if ( key.equals("Rename View"))
		{
			if ( renameView != null )
			{
				renameView.removeAll();
				remove(renameView);
			}
			renameView=new RenameView(this);
			add(renameView, RenameView.NAME);
		}
		
		if ( key.equals("Free Rename View"))
		{
			freeRenameView.removeAll();
			remove(freeRenameView);
			freeRenameView=new FreeRenameView(this);
			add(freeRenameView, FreeRenameView.NAME);
		}
		
		if ( key.equals("Option View"))
		{
			optionView.removeAll();
			remove(optionView);
			optionView=new OptionView(this);
			add(optionView, OptionView.NAME);
		}
		
		if ( key.equals("Analyze View"))
		{
			if ( analyzeView != null )
			{
				analyzeView.removeAll();
				remove(analyzeView);
			}
			
			analyzeView=new AnalyzeView(this);
			add(analyzeView, AnalyzeView.NAME);
		}

		cardLayout.show(this, key);
		repaint();
		revalidate();
	}

}
