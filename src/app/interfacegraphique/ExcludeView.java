package app.interfacegraphique;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import javax.swing.BorderFactory;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;

import app.model.Exclusions;

public class ExcludeView extends JPanel
{
	private static final long serialVersionUID = 1L;
	
	public static final String NAME = "Exclude View";
	private JPanel actionPanel;
	
	public ExcludeView (View view)
	{
		super(new FlowLayout(FlowLayout.CENTER, 500, 10));
		JButton home = new JButton(new GoToAction("Menu principal", MenuView.NAME,view));	
		add(home);
		DefaultListModel<String> listModel = refreshExcludedList();
		if ( actionPanel != null)
			remove(actionPanel);
		actionPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 20, 10));
        setBorder(BorderFactory.createTitledBorder("Gestion des exclusions pour renommage"));
        
		if ( listModel.size() != 0)
		{
			JList<String> ADcomputers = new JList<String>(listModel);
			JScrollPane scrollComputer = new JScrollPane(ADcomputers);
			
			if ( listModel.size()*18 < 400 )
				scrollComputer.setPreferredSize(new Dimension(200,listModel.size()*18));
			else
				scrollComputer.setPreferredSize(new Dimension(200,450));
			
			JButton delete = new JButton("Lever l'exclusion");
			
			delete.addActionListener(new ActionListener() 
			{
				public void actionPerformed(ActionEvent event) 
				{
					int[] selectedIx = ADcomputers.getSelectedIndices();
					
					ArrayList<String> excluded =  new ArrayList<String>();
					
					for (int i = 0; i < selectedIx.length; i++) 
				    {
					      String sel = ADcomputers.getModel().getElementAt(selectedIx[i]);
					      excluded.add(sel);
					     
				    }
					Exclusions.remove(excluded);
					home.doClick();
				}
			});
			
			add(new JLabel(" Listes des postes exclus pour le renommage "));
			actionPanel.add(scrollComputer);
			add(actionPanel);
			add(delete);
			
		}		
		else
		{
			add(new JLabel(" Il n'y a aucun poste exclu pour le renommage "));
		}
		repaint();
		revalidate();
		//view.add(this);
		//setVisible(true);
	}
	
	public DefaultListModel<String> refreshExcludedList()
	{
		ArrayList<String> excluded = Exclusions.getExcluded();
		DefaultListModel<String> listModel =  new DefaultListModel<String>();
		for ( String e : excluded)
		{
			listModel.addElement(e);
		}
		return listModel;
	}

}
