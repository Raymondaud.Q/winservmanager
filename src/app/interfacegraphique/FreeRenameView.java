package app.interfacegraphique;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

public class FreeRenameView extends JPanel  
{
	private static final long serialVersionUID = 1L;
	final public static String NAME = "Free Rename View";
	
	public FreeRenameView (View view)
	{
		super(new FlowLayout(FlowLayout.CENTER, 500, 10));
		JFrame parent = (JFrame) SwingUtilities.getWindowAncestor(this);
		JButton home = new JButton(new GoToAction("Menu principal", MenuView.NAME,view));
        setBorder(BorderFactory.createTitledBorder("Renommer un ordinateur librement"));
		add(home);

        Font guessFont = new Font("SansSerif", Font.BOLD, 20);
		JTextField oldN = new JTextField("");
		JTextField newN = new JTextField("");
		oldN.setPreferredSize(new Dimension(350,40));
		newN.setPreferredSize(new Dimension(200,40));
		oldN.setFont(guessFont);
		newN.setFont(guessFont);
		
		oldN.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			    if ( oldN.getText().length() >= 30 ) // limit to 3 characters
			    	oldN.setText(oldN.getText().substring(0, 29));
			}
		});
		
		newN.addKeyListener(new KeyAdapter() 
		{
			@Override
			public void keyTyped(KeyEvent e) 
			{
			    if ( newN.getText().length() >= 15 ) // limit to 3 characters
			    	newN.setText(newN.getText().substring(0, 14));
			}
		});
		JButton rename = new JButton("Valider");
		
	    rename.addActionListener(new ActionListener()
		{
			@Override
			public void actionPerformed(ActionEvent event)
			{
				if ( oldN.getText() == null || oldN.getText().equals("") )
					JOptionPane.showMessageDialog(view,"Veuillez saisir le nom du poste cible � renommer","Cible ind�finie ",JOptionPane.WARNING_MESSAGE);
				else if ( newN.getText() == null || newN.getText().equals("") ||  newN.getText().length() < 2 )
					JOptionPane.showMessageDialog(view,"Veuillez saisir un nouveau nom de poste (2 caract�res minimum) ","Nom ind�fini ",JOptionPane.WARNING_MESSAGE);
				else
				{
					ArrayList<String> currentName = new ArrayList<String>();
					ArrayList<String> newName = new ArrayList<String>();
					currentName.add(oldN.getText());
					newName.add(newN.getText());
					new ValidationBox(parent,view,currentName,newName);
				}
			}
		});
	    
		JLabel oldLab = new JLabel(" Le nom du poste � renommer : ");
		JLabel newLab = new JLabel(" Nouveau nom : ");
		add(oldLab);
		add(oldN);
		add(newLab);
		add(newN);
		add(rename);
	}
}
