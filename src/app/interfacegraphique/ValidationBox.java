package app.interfacegraphique;


import javax.swing.*;


import powershell.Command;

import java.awt.Frame;
import java.awt.event.*;
import java.util.ArrayList;  
public class ValidationBox extends WindowAdapter
{  
	
	// Pour avoir les noms des postes  : Get-ADComputer -Filter * | Select -Expand Name
	// Pour renomer un Poste a distance : Rename-computer –computername “computer” –newname “newcomputername” –domaincredential domain-user –force –restart
	
	JFrame currentFrame;
	ArrayList<String> newNames;
	ArrayList<String> oldNames;
	
	public ValidationBox(JFrame frame, View view, ArrayList<String> old , ArrayList<String> news )
	{  
		currentFrame = frame ;
		newNames = news;
		oldNames = old;

		String message=" Voulez-vous vraiment effectuer ces opérations de renommage ? \n";
		for ( int i = 0 ; i < newNames.size() ; i ++)
		{
			message += oldNames.get(i)+ " ⇨ " + newNames.get(i) +"\n";
		}
		
	    int a=JOptionPane.showConfirmDialog(currentFrame,message);  
		if(a==JOptionPane.YES_OPTION)
		{
			String callback = Command.renameComputers(news, old);
			
			if ( MenuView.showLog())
			{
				if ( callback.equals(""))
					callback = "Renommage terminé avec succès. \n Toutes les opération on été effectuées correctement";
				showFullAnswer(frame,callback);
			}
			else
				showShortAnswer(frame,callback);
			
			new JButton(new GoToAction("Menu principal", MenuView.NAME,view)).doClick();
		}
		else
			JOptionPane.showMessageDialog(frame,"Opération annulée");

		
	}
	
	private void showFullAnswer( Frame frame,String longMessage) 
	{
	    JTextArea textArea = new JTextArea(25, 50);
	    textArea.setText(longMessage);
	    textArea.setEditable(false);
	    JScrollPane scrollPane = new JScrollPane(textArea);
	    JOptionPane.showMessageDialog(frame, scrollPane);
	}
	
	private void showShortAnswer(Frame frame,String callback)
	{
		boolean errorCatched = false;
		if ( callback.indexOf("0x80070005") != -1) // Wrong cred
		{
			JOptionPane.showMessageDialog(frame,"Mot de passe ou identifiant incorrect. \n Verifiez que vous ayez les droits requis. \n Aucune des opérations n'a pu être éffectuée.","ECHEC : Identifiants incorrects",JOptionPane.WARNING_MESSAGE);
			return;
		}
		if ( callback.indexOf("AddressResolutionException,Microsoft.PowerShell.Commands.RenameComputerCommand") != -1)
		{	
			JOptionPane.showMessageDialog(frame,"Un ou plusieurs postes sont inaccessibles. \n Verifiez qu'ils soient bien allumés et connectés au réseau","ECHEC : Postes injoignables",JOptionPane.WARNING_MESSAGE);
			errorCatched = true;
		}
		if ( callback.indexOf("0x800706BA") != -1 ) // Computer off
		{
			JOptionPane.showMessageDialog(frame,"Un ou plusieurs postes sont inaccessibles. \n Verifiez qu'ils soient bien allumés et connectés au réseau","ECHEC : Postes injoignables",JOptionPane.WARNING_MESSAGE);
			errorCatched = true;
		}	
		if ( callback.indexOf("Skip computer") != -1 )	// invalid name
		{
			JOptionPane.showMessageDialog(frame,"Un ou plusieurs nouveaux nom de postes sont invalides. \n Veuillez utiliser uniquement des lettres, des chiffres et des traits d'union.","ECHEC : Noms de postes invalides",JOptionPane.WARNING_MESSAGE);
			errorCatched = true;
		}	
		if ( callback.indexOf("Computer name") != -1 ) // name not found 
		{
			JOptionPane.showMessageDialog(frame,"Un ou plusieurs postes n'existent pas dans l'Active Directory.","ECHEC : Poste inexistant",JOptionPane.WARNING_MESSAGE);
			errorCatched = true;
		}
		if ( callback.indexOf("ObjectNotFound: (Rename-computer:String)") != -1 )
		{
			JOptionPane.showMessageDialog(frame,"Aucun PowerShell pour ActiveDirectory trouvé sur la machine. \n Aucune des opérations n'a pu être éffectuée.","ECHEC : PowerShell ActiveDirectory introuvable",JOptionPane.WARNING_MESSAGE);
			return;
		}
		
		if (  ! callback.equals("") && ! errorCatched )
			JOptionPane.showMessageDialog(frame, "Une ou plusieurs commandes ont échouées. \n Pour plus de détails activez les messages d'erreur complets dans le menu principal.","ECHEC : Des opérations ont échouées",JOptionPane.WARNING_MESSAGE);
		
		if ( callback.equals("") )
			JOptionPane.showMessageDialog(frame,"Renommage terminé avec succès. \n Toutes les opération on été effectuées correctement");
			
		
	}

}
