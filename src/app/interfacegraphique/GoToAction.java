package app.interfacegraphique;

import java.awt.event.ActionEvent;
import javax.swing.AbstractAction;



public class GoToAction extends AbstractAction
{
    // Class GoToAction that permits to change the panel into the View's CardLayout
	private static final long serialVersionUID = 1L;
	private String key;
    private View myMain;

    /**
     * @author Quentin Raymondaud
     * [GoToAction description]
     * Action that sets the user view to the key panel
     * @param name   String
     * @param key    String
     * @param myMain View
     */
    public GoToAction(String name, String key, View myMain)
    {
        super(name);
        this.key = key;
        this.myMain = myMain;
    }

    @Override
    public void actionPerformed(ActionEvent e)
    {
    	myMain.showCard(key);
    }
}
