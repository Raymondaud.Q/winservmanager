package app.interfacegraphique;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.Vector;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.filechooser.FileSystemView;
import javax.swing.table.DefaultTableModel;

import powershell.Command;

public class AnalyzeView extends JPanel
{
	private static final long serialVersionUID = 1L;
	final public static String NAME = "Analyze View";

	private Vector<String> compute;
	private Vector<String> doubleCSV;
	private Vector<String> notInCSV;
	private Vector<String> notInAD;
	private JPanel resultDisplay;
	
	public AnalyzeView (View view)
	{
		super(new FlowLayout(FlowLayout.CENTER, 1000, 10));
		
		compute = Command.getComputers();
		
		//JFrame parent = (JFrame) SwingUtilities.getWindowAncestor(this);
		JButton home = new JButton(new GoToAction("Menu principal", MenuView.NAME,view));
		Font fieldFont = new Font("Arial", Font.PLAIN, 20);
		JButton chooseCSV = new JButton("Choisir un fichier CSV inventaire");
		chooseCSV.setPreferredSize(new Dimension(330,50));
        chooseCSV.setFont(fieldFont);
        
		chooseCSV.addActionListener(new ActionListener() 
		{
			public void actionPerformed(ActionEvent e) 
            {
				doubleCSV = new Vector<String>();
				if (resultDisplay != null)
				{
					resultDisplay.removeAll();
					remove(resultDisplay);
				}
				resultDisplay = new JPanel();
            	JFileChooser jfc = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
				int returnValue = jfc.showOpenDialog(null);
				if (returnValue == JFileChooser.APPROVE_OPTION) 
				{
					File selectedFile = jfc.getSelectedFile();
					Vector<String> records = new Vector<String>();
					InputStream in;
					try 
					{
						in = new FileInputStream(selectedFile.getAbsolutePath());
						@SuppressWarnings("resource")
						Reader r = new InputStreamReader(in, "US-ASCII");
						int intch;
						boolean firstLine = true;
						int nbSemiColon = 0;
						int scIndex = 0;
						String buffer = "";
						String fieldBuffer = "";
						ArrayList<String> titles = new ArrayList<String>();
						String[] tmpRow = null ;
						DefaultTableModel datas = null;
						int serialField = 0 ;
						int modelField = 0 ;
						while ((intch = r.read()) != -1) 
						{
							char ch = (char) intch;
							
							if ( firstLine )
							{
								if (ch == ';')
								{
									fieldBuffer = fieldBuffer.replace("\"", "");
									titles.add(fieldBuffer);
									
									 System.out.println(fieldBuffer);
									
									if ( fieldBuffer.equals("Mod��le"))
										serialField = nbSemiColon ;
									if ( fieldBuffer.equals("Num��ro de s��rie"))
										modelField = nbSemiColon ;
									nbSemiColon += 1;
									fieldBuffer = "";
								}
								else if ( ch == '\n' )
								{
									firstLine = false;
									fieldBuffer = "";
									String[] tit = titles.toArray(new String[titles.size()]);
									datas = new DefaultTableModel(tit, 0);
								}
								else
									fieldBuffer += ch;
							}
							
							else
							{
								if ( tmpRow == null )
									tmpRow = new String[nbSemiColon];
								
								if (ch == ';' && fieldBuffer.charAt(fieldBuffer.length()-1) == '"')
								{
									tmpRow [scIndex] = fieldBuffer;
									scIndex += 1;
									buffer += ch;
									fieldBuffer = "";
									
								}
								else if (ch != '\n')
								{
									buffer += ch;
									fieldBuffer += ch;
								}
								
								if ( scIndex == nbSemiColon )
								{
									scIndex = 0 ;
									records.add(buffer);
									buffer = "";
									datas.addRow(tmpRow);
									tmpRow = null;
								}
							}
						}
						
						
					    
						JTable table = new JTable(datas);
						table.setAutoCreateRowSorter(true);
						JFrame frame = new JFrame();
					    frame.add(new JScrollPane(table));
					    frame.pack();
					    frame.setLocationRelativeTo(null);
					    frame.setVisible(true);
					    
					    doubleCSV = searchDouble(serialField,modelField,datas);
						notInCSV = searchDiff(records,compute);
						notInAD = searchDiff(compute,records);
					}
					catch (Exception e1) 
					{
						e1.printStackTrace();
					}
					
					JScrollPane scrollDouble = new JScrollPane(new JList<String>(doubleCSV));
					JScrollPane scrollNotInCSV = new JScrollPane(new JList<String>(notInCSV));
					JScrollPane scrollNotInAD = new JScrollPane(new JList<String>(notInAD));
					
					scrollDouble.setPreferredSize(new Dimension(200,450));
					scrollNotInCSV.setPreferredSize(new Dimension(200,450));
					scrollNotInAD.setPreferredSize(new Dimension(200,450));
					
					resultDisplay.add(scrollDouble);
					resultDisplay.add(scrollNotInCSV);
					resultDisplay.add(scrollNotInAD);
					add(resultDisplay);
					repaint();
					revalidate();
				}
            }
		});
		add(home);
		add(chooseCSV);
		add(new JLabel("Doublons CSV :                 		                Absents CSV : 	           	                        Absents AD :"));
	}
	
	public Vector<String> searchDouble(int serial, int model, DefaultTableModel dm)
	{
		Vector<String> result = new Vector<String>();
		for(int compRow = 0;compRow < dm.getRowCount()-1;compRow++)
		{
			for(int row = compRow+1 ; row < dm.getRowCount()-1 ; row++) 
			{
			    for(int col = 0 ; col < dm.getColumnCount(); col++) 
			    {
			       if ( dm.getValueAt(compRow, serial).equals(dm.getValueAt(row, serial)) && dm.getValueAt(compRow, model).equals(dm.getValueAt(row, model )))
			       {
			    	   String e = (String)dm.getValueAt(compRow, 0);
			    	   if ( ! alreadyIN(result,e))
			    		   result.add(e);
			       }
			    }
			}
		}
		return result;
	}
	
	public boolean alreadyIN( Vector<String> m , String e)
	{
		for ( String c  : m )
			if ( c.equals(e))
				return true;
		return false;
	}
	
	public Vector<String> searchDiff ( Vector<String> in , Vector<String> wanted )
	{
		Vector<String> result = new Vector<String>();
		boolean found = false;
		for ( String c : wanted )
		{
			found = false;
			
			c = c.replace("\"", "");
			c = c.replace(" ", "");
			if ( c.indexOf(';') != -1 )
				c = c.substring(0, c.indexOf(';'));
			
			if ( c.indexOf(';') != -1 )
				c = c.substring(0, c.indexOf(';'));
			
			for ( String b : in)
			{
				b = b.replace("\"", "");
				b = b.replace(" ", "");
				
				if ( b.indexOf(';') != -1 )
					b = b.substring(0, b.indexOf(';'));

				if ( b.indexOf(c) != -1 )
				{
					found = true;
					break;
				}
				
				// System.out.println( " VOILA LA COMPARAISON : " + b +" : "+ c);
			}
			
			if ( found == false )
			{
				result.add(c);
			}
		}
		return result;
	}
}
