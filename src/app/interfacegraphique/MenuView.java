package app.interfacegraphique;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionAdapter;
import java.awt.image.BufferedImage;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.TitledBorder;

public class MenuView extends JPanel  implements MouseListener
{

	private static final long serialVersionUID = 1L;
	final public static String NAME = "Menu Principal - Renommage Windows Server";
	private static boolean showLog = false;
	
	Image selectedImage;
	Thread gameThread;
	
	boolean posFlag = false;
	boolean gameLaunched = false;
	
	int circleX = 860;
	int circleY = 680;
	int circleVX = 0;
	int circleVY = 0;
	int sCircle = 10;
	
	int x = 25 ;
	int y = 25 ;
	int vx = 0;
	int vy = 0;
	int sLogo = 150;
	
	int mx = 0;
	int my = 0;
	
	@Override
	public void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		selectedImage = new ImageIcon(".\\logo.jpg").getImage();
		//Image -> BufferedImage code
		BufferedImage img = new BufferedImage(selectedImage.getWidth(null), selectedImage.getHeight(null), BufferedImage.TYPE_INT_ARGB);
		Graphics2D gg = img.createGraphics();
		
		gg.drawImage(selectedImage, 0, 0, null);
		gg.dispose();

		//set each pixel to be transparent
		int transparency = 200 ; //0-255, 0 is invisible, 255 is opaque
		int colorMask = 0x00FFFFFF; //AARRGGBB
		int alphaShift = 24;
		for(int y = 0; y < img.getHeight(); y++)
		    for(int x = 0; x < img.getWidth(); x++)
		        img.setRGB(x, y, (img.getRGB(x, y) & colorMask) | (transparency << alphaShift));
		g.drawImage(img, x, y, sLogo, sLogo,null);
		
		if ( gameLaunched)
		{
			if ( 2*sCircle >= 140 )
				g.setColor(Color.RED);
			g.drawOval( circleX+1, circleY, 2*sCircle , 2*sCircle);
			g.setColor(Color.BLACK);
		}
		
		revalidate();
		repaint();
	}
	
	public MenuView(View myMain)
	{
		setLayout(new FlowLayout(FlowLayout.CENTER, 1000, 30));
        JButton rename = new JButton(new GoToAction("Renommer des PC", RenameView.NAME, myMain));
        JButton exclude = new JButton(new GoToAction("Exclusions", ExcludeView.NAME, myMain));
        JButton freeRename = new JButton(new GoToAction("Renommage libre", FreeRenameView.NAME, myMain));
        JButton options = new JButton(new GoToAction("Options", OptionView.NAME, myMain));
        JButton analyze = new JButton(new GoToAction(" Active Directory VS CSV ", AnalyzeView.NAME, myMain));
        JButton ex = new JButton("Quitter");
        ex.addActionListener(new ActionListener()
        {
            @Override
			public void actionPerformed(ActionEvent event)
            {
                int clickedButton = JOptionPane.showConfirmDialog(null, "Voulez-vous vraiment quitter ?", "Confirmation de sortie", JOptionPane.YES_NO_OPTION);
                if (clickedButton == JOptionPane.YES_OPTION)
                {
                    System.exit(0);
                }
            }
        });
        
        Font fieldFont = new Font("Arial", Font.PLAIN, 20);
        
        rename.setFont(fieldFont);
        exclude.setFont(fieldFont);
        freeRename.setFont(fieldFont);
        options.setFont(fieldFont);
        ex.setFont(fieldFont);
        analyze.setFont(fieldFont);

        setBorder(BorderFactory.createTitledBorder(null, NAME, TitledBorder.CENTER, TitledBorder.TOP, new Font("times new roman",Font.BOLD,15), Color.black));
       
        freeRename.setPreferredSize(new Dimension(330,50));
        rename.setPreferredSize(new Dimension(330,50));
        exclude.setPreferredSize(new Dimension(330,50));
        options.setPreferredSize(new Dimension(330,50));
        analyze.setPreferredSize(new Dimension(330,50));
        JCheckBox show = new JCheckBox("Afficher les r�ponses completes",false); 
        show.addActionListener(new ActionListener() 
        {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
            	showLog = show.isSelected();
            }
        });
        
        addMouseListener(this);
        addMouseMotionListener(new MouseMotionHandler());
        add(show);
		add(rename);
		add(freeRename);
		add(exclude);
		add(analyze);
		add(options);
        add(ex);
	}
	
	public static boolean showLog()
	{
		return showLog;
	}
	@Override
	public void mouseClicked(MouseEvent e) {}
	@Override
	public void mouseEntered(MouseEvent e){}
	@Override
	public void mouseExited(MouseEvent e) 
	{}
	@Override
	public void mousePressed(MouseEvent e) 
	{
	if ( ! gameLaunched )
	{
		gameLaunched = true;
			
		if (gameThread != null )
			gameThread.interrupt();
			
		gameThread =  new Thread(new Runnable() 
		{
			public void run() 
			{
				
				while (! Thread.interrupted() && gameLaunched )
				{
			    	try 
			    	{
						Thread.sleep(10);
					} catch (InterruptedException e) 
			    	{
						System.out.println("Reinit");
					}
				    /* PHYSIQUE DU LOGO */	
			    	if ( mx - x < -5 && vx >= -100 )
						vx+=-5;
			    	else if (  mx - x > 5 && vx <= 100)
						vx+=5;
				    		
			    	if ( my - y <= -5 && vy >= -100 )
						vy+=-5;
					else if  (  my - y > 5 && vy <= 100)
						vy+=5;
			    		
			    	if ( x >= 900-sLogo || x < 0 )
			    		vx = -vx;
			    	if ( y >= 720-sLogo || y < 0)
			    		vy = -vy;
			    		
			    	if ( vx > 0 && x < 900-sLogo)
						x+=vx/10;
					else if ( vx < 0 && x > 0 )
						x+=vx/10;
			    		
					if ( vy > 0 && y < 720-sLogo)
						y+=vy/10;
					else if ( vy < 0 && y > 0)
						y+=vy/10;
					
					if ( vx > 0 )
						vx -=1;
					else if ( vx < 0)
						vx += 1;
					if ( vy > 0 )
						vy -=1;
					else if ( vy < 0)
						vy += 1;
					
					/* PHYSIQUE DU CERCLE */
					if ( x + ( sLogo/2 ) - sCircle - circleX < -5 && circleVX >= -100 )
						circleVX += -5;
			    	else if (  x + ( sLogo/2 ) - sCircle - circleX > 5 && circleVX <= 100)
			    		circleVX += 5;
				    		
			    	if ( y + ( sLogo/2 )-sCircle - circleY < -5 && circleVY >= -100 )
						circleVY += -5;
					else if  ( y + ( sLogo/2 ) - sCircle - circleY > 5 && circleVY <= 100)
						circleVY += 5;
			    		
			    	if ( circleX >= 900 - sCircle*2 || circleX < 0 )
			    		circleVX = - circleVX;
			    	if ( circleY >= 720 - sCircle*2 || circleY < 0)
			    		circleVY = - circleVY;
			    		
			    	if ( circleVX > 0 && circleX < 900-sCircle*2)
						circleX+=circleVX/12;
					else if ( circleVX < 0 && circleX > 0 )
						circleX+=circleVX/12;
			    		
					if ( circleVY > 0 && circleY < 720-sCircle*2)
						circleY+=circleVY/10;
					else if ( circleVY < 0 && circleY > 0)
						circleY+=circleVY/10;
					
					if ( circleVX > 0 )
						circleVX -=1;
					else if ( circleVX < 0)
						circleVX += 1;
					if ( circleVY > 0 )
						circleVY -=1;
					else if ( circleVY < 0)
						circleVY += 1;
					
					if ( (circleX - x < sLogo  && circleX - x > -sCircle*2)  && ( circleY - y < sLogo && circleY - y > -sCircle*2  ) )
					{
						if ( sLogo > 0  )
						{
							vx = vx*-2;
					    	vy = vy*-2;
					    	circleVX = circleVX*-2;
					    	circleVY = circleVY*-2;
							sLogo -= 2;
							sCircle += 1;
						}
					}	
					
					repaint();
				}
			}
		});
		gameThread.start();
		}
		else
		{
			if (gameThread != null )
				gameThread.interrupt();
			
			gameLaunched = false;
			posFlag = false;
			vx = 0;
			vy = 0;
			x = 25;
			y = 25;
			sLogo = 150;
			sCircle = 10;
			circleX = 860;
			circleY = 680;
			circleVX = 0;
			circleVY = 0;
		}
		repaint();
  }

  @Override
  public void mouseReleased(MouseEvent e) {}
	class MouseMotionHandler extends MouseMotionAdapter 
	{
		public void mouseMoved(MouseEvent e) 
		{
			mx = e.getX()-(sLogo/2);
			my = e.getY()-(sLogo/2);
		}
	}
}

