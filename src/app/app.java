package app;

import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.SwingUtilities;
import app.interfacegraphique.View;
import app.model.Exclusions;

public class app 
{
	public static void main(String[] args)
	{
		SwingUtilities.invokeLater(() -> { createAndShowGui();});
	}

	/**
	 * @author Raymondaud Quentin
	 * [createAndShowGui description]
	 * Creates a View, a frame, etc ..
	 * and shows it
	 */
	private static void createAndShowGui()
	{
		View mainPanel = new View();
		JFrame frame = new JFrame("Server Manager");
		frame.setDefaultCloseOperation(0);
		frame.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				int clickedButton = JOptionPane.showConfirmDialog(null, " Voulez-vous quitter ?", "Confirmation", JOptionPane.YES_NO_OPTION);
				if (clickedButton == JOptionPane.YES_OPTION)
				{
					System.exit(0);
				}
			}
		});
		//frame.setResizable(false);
		JScrollPane scrPane = new JScrollPane(mainPanel);
		frame.add(scrPane);
		frame.setIconImage(new ImageIcon(".\\logo.jpg").getImage());
		//frame.add(mainPanel);
		frame.pack();
		frame.setLocationByPlatform(true);
		frame.setVisible(true);
		Exclusions.load();
		
	}
}
