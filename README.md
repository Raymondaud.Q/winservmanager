# WinServManager

**Projet logiciel stage Administrateur réseau LEGTA Louis Giraud Carpentras fin L3 :**

Conçue par *Mohammed.A* technicien régional maintenance réseau et consultant à son compte, *Damien.G* administrateur réseau et *Quentin.R* étudiant.

Developpée par *Quentin.R*

Vous pouvez faire ce que vous voulez de cette application, mais je ne suis en aucun cas responsable de l'utilisation que vous pourriez en faire.
 
Cette application permet lorsqu'elle est installée sur une machine connectée a un serveur Windows de lister et renommer en masse les machines du réseau.

Elle a été réalisé en Java et utilise Swing pour l'interface graphique.

Une option d'exclusion a été intégrée pour interdire le renommage de certaines machines

Cette application effectue des requetes PowerShell ( >= 3.0 )  pour le renommage et le listing des postes.

**Installation**

Vous pouvez cloner le dépots et ses branches puis compiler et créer l'archive exécutable JAR vous même.

Vous pouvez aussi utiliser les archives présentes sur le dépot [GitLab du projet](https://gitlab.com/CERI_Raymondaud.Q/winservmanager/) dans le dossier ServerManager: 

>  WinServRenamer.jar = V1

>  WinServRenamerV2.jar = V2

>  WinServRenamerV3.jar = V3

Une fois l'archive JAR obtenue, assurez vous d'avoir :

* V2 : une image de votre choix au format JPEG se nommant "logo" dans le même repertoire que le JAR

* une JRE version 8 ou plus

* un PowerShell version 3 ou plus ainsi que son module ActiveDirectory sur votre machine ( Windows Server ).

**3 Type de renommages :** 
        	
*  Renommage par pattern  ( plusieurs ou un seul poste ):
	
		- Saisie de 13 caractères  suivit d'un tiret et le numéro du poste dans la liste des renommage.
Exemple pour la saisie de "exemple-reno-" : 

> posteAdmin devient exemple-reno-1
> 
> posteSecr  devient exemple-reno-2
> 
> ...
	
*  Renommage individuel ( plusieurs ou un seul poste ) :
	
		- Saisie de 15 caractères maximum par machine séléctionnée pour renommage.
Permet des noms différents pour chaques machines.
		
		
*	Renommage libre ( un seul poste ) :
	
		- Saisie du nom actuel de la machine cible ( 30 caractères maximum ).
		- Saisie du nouveau nom ( 15 caractères maximum ) .

Le panneau de renommage affichant la liste des postes actuelle propose une option d'inversion de poste.
Cette option est utile pour oragniser les poste pour un renommage par pattern.
Cette option fait avancer l'element i ( ou reculer l'element i+1 ) si il n'y a qu'un seul poste séléctionné.
Si deux postes ou plus sont selectionné cette option inverse le premier et le deuxième poste selectionné.
Si aucun poste n'est selectionné alors il ne se passe rien.
		
Option utile dans le cas ou le nom fournit par serveur ne correspond pas à celui dans l'Active Directory.
La commande PowerShell qui récupères le nom des postes ne récupère que les 15 premiers caractères.
Lorsqu'ils en font plus de  15 il est impossible de le renommer sachant que le nom de la machine cible est incomplet.
Ce panneau est donc utile pour renommer n'importe quelle machine du réseau ayant été renommé avec un nom trop long.
		
**Les exclusions :**

	Tout les postes ajoutés a la liste des exclusions ne seront plus disponibles pour renommage ( sauf libre ).
	Il est possible d'ajouter et de supprimer des exclusions.
	
**Les options (V2) :**

    Vous pouvez maintenant choisir la position de l'incrément dans le renommage par pattern, vous pouvez le positionner du premier au treizième caractère.
    Vous pouvez également choisir le départ de l'incrément, c'est à dire de de commencer par example avec un incrément égale à 13 puis suivre 14 , 15 , ...
    Une visualisation dynamiqe du pattern configuré est a votre disposition dans le menu de renommage et d'options, vous pouvez donc voir le pattern que vous avez créé .

**Le comparateur Active Directory VS CSV (V3) :**
    
    Ce module permet de comparer l'export en CSV de l'inventaire GLPI aux machines présentes dans l'Active Directory
    
    Il liste :
        Les doublons dans l'inventaie GLPI.
        Les machines non présentes dans l'inventaire GLPI mais présentes dans l'Active Directory.
        Les machines non présentes dans l'Active Directory mais présentes dans l'export CSV.
    
    Ces compraisons permettent de savoir si un PC est référencé dans l'inventaire mais pas connecté au réseau ( Active Directory );
    Ou encore de savoir si un PC est monté sur le réseau mais pas référencé dans l'inventaire et enfin de savoir si des PC sont référencés en double dans le fichier CSV
    
	
**Validation des/du renommage/s :**

	
    Lors de la validation des renommages, l'utilisateur devra se connecter sur une session ayant les droits correspondant sur le serveur.
    	
    V1 : Il devra donc saisir "NomDeDomaine\Nomd'Utilisateur" dans une première fenêtre puis valider.
    
    Il devra  vérifier et valider le renommage avant de le lancer grâce à une boîte de dialogue.
    
    V2 : Enfin il devra s'identifer grâce à la boîte de dialogue ouverte par le PowerShellAD afin de terminer le renommage.
    
    V1 : Il devra rentrer son mot de passe pour chaques Poste ( si il y 25 postes a renommer il faudra l'entrer 25 fois ).
    
    Lorsque tous les renommages sont validés une petite fenêtre s'ouvre affichant les log si l'option a été selectionnée.
    Elle indiquera pour chacun des postes sur lesquels on a lancer un renommage si il y a eu une erreure ou pas.





		
		
		
	